<?php

class Model_register extends MY_Model {

    public $error = [];
    private $_tb_register = ['card_type', 'card_no', 'national', 'email', 'password','mail_address'];
    private $_tb_register_second = ['name_eng', 'name_thai', 'date_of_birth',
        'place_of_birth', 'permanent_address', 'province', 'district', 'zipcode',
        'telephone', 'area_code', 'mobile', 'mail_address', 'title_name_contact','name_contact',
        'address_contact', 'telephone_contact', 'mobile_contact', 'relationship'];

     public function __construct() {
        parent::__construct();
    }

    public function getRegister($id) {
        $this->db->where('register_key', $id);
        $this->db->limit(1);
        $sql = $this->db->get('register');
        return $sql->row();
    }

    public function updateRegister($params, $type = 'update', $step='first')
    {
        $act = FALSE;
        $register_id = $params['register_id'];

        $table = ( $step=='first' ? $this->_tb_register : $this->_tb_register_second);
        $update = $this->filterTBRegister($params, $table);
        unset($params);

        if (!empty($register_id)) {
            // =================================== UPDATE ================================
            $this->db->where('register_id', $register_id)->update('register' , $update['register']);
            $act = $register_id;

        } else if ($type == 'insert') {
            // =================================== INSERT ================================
            if (!empty($update['register'])) {
                $this->db->insert('register' , $update['register']);
                $act = $this->db->insert_id();
            }
        }
        return $act;
    }

    private function filterTBRegister($params, $table) {
        $update = array(
            'register' => [],
        );
        $fill_content = array_fill_keys($table, 0);
        $update['register'] = array_intersect_key($params, $fill_content);
        return $update;
    }

    public function update_custom_field($id, $field, $value)
    {
        $this->db->where('register_id', $id)->update('register', [$field =>$value]);
    }
}