<?php

class Model_register_fourth extends MY_Model {

    public $error = [];
    private $_tb_register = ['register_id', 'period_from', 'period_to', 'school_name', 'state_province', 'country', 'relate_document', 'additional_experiences'];

     public function __construct() {
        parent::__construct();
    }

    public function getRegister($id) {
        $this->db->where('register_key', $id);
        $this->db->limit(1);
        $sql = $this->db->get('register');
        return $sql->row();
    }

    public function getRegisterEX($id) {
        $this->db->where('register_id', $id);
        $this->db->limit(1);
        $sql = $this->db->get('register_experience');
        return $sql->row();
    }

    public function getAwards($id) {
        $this->db->where('register_id', $id);
        $sql = $this->db->get('register_awards');
        return $sql->result();
    }

    public function updateRegister($params, $type = 'update')
    {
        $act = FALSE;
        $experience_id = $params['experience_id'];
        $register_id = $params['register_id'];
        $awards_list = $params;

        $update = $this->filterTBRegister($params, $this->_tb_register);
        unset($params);

        if (!empty($experience_id)) {
            // =================================== UPDATE ================================
            $this->db->where('experience_id', $experience_id)->update('register_experience' , $update['register']);
            $act = $experience_id;

            //update awards
            $this->update_awards($register_id, $awards_list, 'exists');
            unset($awards_list);

        } else if ($type == 'insert') {
            // =================================== INSERT ================================
            if (!empty($update['register'])) {
                $this->db->insert('register_experience' , $update['register']);
                $act = $this->db->insert_id();

                //insert awards
                $this->update_awards($register_id, $awards_list, 'new');
                unset($awards_list);
            }
        }
        return $act;
    }

    private function update_awards($register_id, $awards, $status){
        //delete awards exists
        if($status == 'exists' && !empty($register_id)){
            $this->db->where('register_id', $register_id);
            $this->db->delete('register_awards');
        }
        //insert
        if(!empty($awards['type_competition']) && !empty($awards['name_institution']) && !empty($awards['name_prize'])){
            for($i=0; $i<count($awards['type_competition']); $i++){
                $data = array();
                $data['type_competition'] = $awards['type_competition'][$i];
                $data['name_institution'] = $awards['name_institution'][$i];
                $data['name_prize'] = $awards['name_prize'][$i];
                $data['date_received'] = $awards['date_received'][$i];
                $data['register_id'] = $register_id;
                $this->db->insert('register_awards' , $data);
            }
        }
    }

    private function filterTBRegister($params, $table) {
        $update = array(
            'register' => [],
        );
        $fill_content = array_fill_keys($table, 0);
        $update['register'] = array_intersect_key($params, $fill_content);
        return $update;
    }

    public function update_file($id, $file)
    {
      $this->db->where('experience_id', $id)->update('register_experience', ['relate_document' =>$file]);
    }
}