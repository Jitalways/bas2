<?php

class Model_register_third extends MY_Model {

    public $error = [];
    private $_tb_register = ['register_id', 'institution_junior', 'city_junior', 'country_junior', 'education_system_junior',
    'year_graduation_junior', 'gpa_junior', 'type_certificate_junior','indicate_scores_junior', 'institution_high', 'city_high', 'country_high', 'education_system_high', 'type_certificate_high','indicate_scores_high' ];

     public function __construct() {
        parent::__construct();
    }

    public function getRegister($id) {
        $this->db->where('register_key', $id);
        $this->db->limit(1);
        $sql = $this->db->get('register');
        return $sql->row();
    }

    public function getRegisterEdu($id) {
        $this->db->where('register_id', $id);
        $this->db->limit(1);
        $sql = $this->db->get('register_education');
        return $sql->row();
    }

    public function updateRegister($params, $type = 'update')
    {
        $act = FALSE;
        $education_id = $params['education_id'];

        $update = $this->filterTBRegister($params, $this->_tb_register);
        unset($params);

        if (!empty($education_id)) {
            // =================================== UPDATE ================================
            $this->db->where('education_id', $education_id)->update('register_education' , $update['register']);
            $act = $education_id;

        } else if ($type == 'insert') {
            // =================================== INSERT ================================
            if (!empty($update['register'])) {
                $this->db->insert('register_education' , $update['register']);
                $act = $this->db->insert_id();
            }
        }
        return $act;
    }

    private function filterTBRegister($params, $table) {
        $update = array(
            'register' => [],
        );
        $fill_content = array_fill_keys($table, 0);
        $update['register'] = array_intersect_key($params, $fill_content);
        return $update;
    }
}