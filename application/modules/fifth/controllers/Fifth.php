<?php

class Fifth extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_register_fifth', 'model');
    }

    public function index()
    {
        $info = NULL;
        $info_member = NULL;
        $param = NULL;

        /* ==================== Query start data ======================= */
        if (!empty($this->uid)) {
           //get info
            $info_member =$this->model->getRegister($this->uid);
            if(!empty($info_member)){
                $info=$this->model->getRegisterSkill($info_member->register_id);
            }
        }
        /* ==================== Submit form data ======================= */
        if ($this->input->post()) {
            $param = $this->input->post();
            $param['register_id'] = $info_member->register_id;

            //update data
            $update_id = $this->model->updateRegister($param , ((!empty($info)) ? 'update' : 'insert'), 'second');
            if($update_id){
                //upload image
                $this->load->model('model_upload');
                $upload_list = array('statement', 'evidence_high_school', 'transcripts', 'evidence_english', 'evidence_showing', 'evidence_prize', 'photo', 'others','evidence_payment');
                foreach($upload_list as $field) {
                    if (!empty($_FILES[$field]['name'])) {
                        $file_name = $this->model_upload->upload_file($field, $info_member->register_id, 'all', $field);
                        if ($file_name !== FALSE) {
                            $this->model->update_file($update_id, $field, $file_name);
                            $this->model_upload->unlink_file($this->input->post('old_'.$field, TRUE));
                        }
                    }
                }
                //send mail
                $this->send_mail();
                redirect('thankyou');
            }else{
                $this->data['error'] = TRUE;
            }
        }

        /* ==================== Render View ======================= */
        $this->data['info'] = $info;
        $this->data['eng_type_test_list'] =  array('TOEFL IBT', 'TOEFL Computer-based system', 'TOEFL Paper-based system', 'New SAT (Reading and Writing)', 'Old SAT (Critical Reading)');
        $this->view('fifth');
    }

    private function send_mail(){
        $this->load->model('Model_register', 'model_first');
        $register_info = $this->model_first->getRegister($this->uid);
        if (!empty($register_info)) {

            $this->load->library('email');
            $subject ="Confirmation of BAS Application 2019";
            $message ="Dear ".$register_info->name_eng.",\n\n\n";
            $message .= "This email is to confirm you have submitted an online application form for the British
and American Studies (International Programme).
If you have any queries please contact us.\n\nBAS Admissions Section";

            $this->email->from('no-reply@basadmission.org', 'BAS Admissions Section');
            $this->email->to($register_info->email);
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();
        }
    }


}
