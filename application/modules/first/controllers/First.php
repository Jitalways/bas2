<?php

class First extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_register', 'model');
    }

    public function index()
    {
       $info = NULL;
       $param = NULL;
        /* ==================== Query start data ======================= */
        if (!empty($this->uid)) {
            //get info
            $info =$this->model->getRegister($this->uid);
        }

        /* ==================== Submit form data ======================= */
        if ($this->input->post()) {
            $param = $this->input->post();
            $param['mail_address']=$param['email'];
            unset($param['confirm_password']);

            $update_id = $this->model->updateRegister($param , ((!empty($info)) ? 'update' : 'insert'));
            if($update_id){

                if(empty($info)){
                    $cookie_value = md5($param['email'].'bas'.$update_id);
                    $this->_setCookie(array('member'=>$cookie_value));

                    //update register key
                    $this->model->update_custom_field($update_id, 'register_key', $cookie_value);
                }
                redirect('second');
            }else{
                $this->data['error'] = TRUE;
            }
        }

        /* ==================== Render View ======================= */
        $this->data['info'] = $info;
        $this->data['national_list'] = json_decode(file_get_contents(base_url() . 'themes/default/config/nationalities.json'));
        $this->view('first');
    }
	
	    private function test(){
        $this->load->model('Model_register', 'model_first');


            $this->load->library('email');
            $subject ="Confirmation of BAS Application 2019";
            $message ="Dear jitkajohnm,\n\n";
            $message .= "This email is to confirm you have submitted an online application form for the British
and American Studies (International Programme).
If you have any queries please contact us.\n\nBAS Admissions Section";

            $this->email->from('no-reply@basadmission.org', 'BAS Admissions Section');
            $this->email->to('jitalway@gmail.com');
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();
    }

}
