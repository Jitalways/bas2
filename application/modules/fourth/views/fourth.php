        <div class="container">
            <div class="signup-content">
                <div class="signup-form" style="margin:0px auto;">
                    <?php
                    if (isset($error)) {
                        echo '<div class="alert alert-danger">เกิดความผิดพลาด! ไม่สามารถบันทึกข้อมูลลงระบบได้</div>';
                    } ?>
                    <form method="POST" action="<?php echo base_url(); ?>fourth" class="register-form" role="form"  id="register-form4" enctype="multipart/form-data">
                        <input type="hidden" name="experience_id" value="<?php echo set_value('experience_id', ((!empty($info->experience_id)) ? $info->experience_id : 0)); ?>">
                        <h2>Application Form ( Step 4. )</h2>
                        <h4>
                            Studen Exchange Programme Abroad
                        </h4>
                        <h5>
                            If you have any experience participating in an international exchange programme abroad in an English-speaking country for more than 6 months, please provide us wit more details.
                        </h5>
                        <br/>
                        <div class="form-row">
                            <div class="form-group">
                                <label for="period_from" class="radio-label">Period attended from  MONTH/YEAR :</label>
                                <input type="text" name="period_from" id="period_from" autocomplete="off"
                                    readonly data-bv-notempty="true"
                                    data-bv-notempty-message="The Period Attended From is  and cannot be empty" value="<?php echo set_value('period_from', ((!empty($info->period_from)) ? $info->period_from : '')); ?>" />
                            </div>
                            <div class="form-group">
                                <label for="period_to" class="radio-label">to MONTH/YEAR : Length of time</label>
                                <input type="text" name="period_to" id="period_to" autocomplete="off"
                                    readonly data-bv-notempty="true"
                                    data-bv-notempty-message="The Period Attended To is  and cannot be empty" value="<?php echo set_value('period_to', ((!empty($info->period_to)) ? $info->period_to : '')); ?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="school_name" class="radio-label">School :</label>
                            <input type="text" name="school_name"  id="school_name" value="<?php echo set_value('school_name', ((!empty($info->school_name)) ? $info->school_name : '')); ?>" />
                        </div>
                        <div class="form-group">
                            <label for="state_province" class="radio-label">State/Province:</label>
                            <input type="text" name="state_province" id="state_province" value="<?php echo set_value('state_province', ((!empty($info->state_province)) ? $info->state_province : '')); ?>"  />
                            </div>
                        <div class="form-row">
                            <div class="form-group">
                                <label for="country" class="radio-label">Country :</label>
                                <div class="form-select">
                                    <select name="country" id="country">
                                        <option value="">-- select one --</option>
                                        <?php
                                        foreach ($country_list as $cvalue) {
                                            echo '<option value="' . $cvalue->name . '" ' . set_select('country', $cvalue->name, ((!empty($info->country) && $info->country == $cvalue->name) ? true : false)) . '>' . $cvalue->name . '</option>';
                                        }
                                        ?>
                                    </select>
                                    <span class="select-icon"><i class="zmdi zmdi-chevron-down"></i></span>
                                </div>
                            </div>
                            <div class="form-group">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="relate_document" class="radio-label">Please upload any related documents (e.g. certificate of participation, transcripts) as evidence of your study abroad program.</label>
                            <div class="form-row">
                                <div class="form-group">
                                    <input type="file" name="relate_document">
                                    <?php if (!empty($info->relate_document)) { ?>
                                        <i class="glyphicon glyphicon-file"><?php echo $info->relate_document; ?></i>
                                     <?php 
                                } ?>
                                    <input type="hidden" name="old_relate_document" value="<?php echo set_value('relate_document', ((!empty($info->relate_document)) ? $info->relate_document : '')); ?>" />
                                </div>
                            </div>
                        </div>

                        <hr/>
                        <br/>
                        <p>
                            <h2>
                                Awards/Prizes received at a national or international level
                            </h2>
                        </p>

                        <div id="div_award">
                            <!-- for first insert -->
                            <?php if (empty($info_awards)) {
                                $this->view("empty_awards", array('node_number' => 1));
                            } else {
                                $this->view("info_awards");
                            } ?>
                        </div>

                        <div class="form-row">
                            <div class="form-group">
                            </div>
                            <div class="form-group">
                                <input type="button" value="Add" id="btnAdd" style="width: 120px; float:right" class="btn btn-success" style="float:right;" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="additional_experiences" class="radio-label">Additional Experiences (if any) </label>
                            <textarea name="additional_experiences" id="additional_experiences" cols="40" rows="5" ><?php echo set_value('additional_experiences', ((!empty($info->additional_experiences)) ? $info->additional_experiences : '')); ?></textarea>
                        </div>
                        <div class="form-submit">
                            <input type="button" value="Back" class="submit btn btn-default" id="reset" title="/third" />
                            <input type="submit" value="Submit Form" class="submit btn btn-primary" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script>
        var template = `<?php $this->view("empty_awards", array('node_number' => '##ID##')); ?>`

            function addAward (idx){
                $('#div_award').append(template.replace('##ID##',idx))
            }

            $(function() {
                var cardtype_text = 'The card_no is not valid'
                // $('#div_award').append(template)
                var i = 1
                $('#btnAdd').on('click',function (){
                    // var clone = template.clone().removeClass('hidden').removeAttr('id').insertAfter(template)
                    $('#div_award').append(addAward(++i))
                    // // Add new field
                    // $('#register-form4').bootstrapValidator('addField', $('#div_award').find('[name="type_competition[]"],[name="name_institution[]"],[name="name_prize[]"],[name="date_received[]"]'));

                    $('.selectDate').datepicker({
                        autoclose: true,
                        format: "yyyy-mm-dd",
                        viewMode: 2
                    }).on('change',function(){
                        $('#register-form4').bootstrapValidator('updateStatus', $('.selectDate'), 'VALIDATED').bootstrapValidator('validateField', $('.selectDate'));
                    });

                    $('#register-form4').find('.txttitle').each(function(e,s) {
                        $(s).text('Panel ' + (e+1))
                    });
                })

                $('#register-form4').on('click', '.removeButton', function() {
                    var row    = $(this).parents('.panel-group')
                    var option = row.find('[name="option[]"]');

                    // Remove element containing the option
                    row.remove();

                    // Remove field
                    // $('#register-form4').bootstrapValidator('removeField', row.find('[name="type_competition[]"],[name="name_institution[]"],[name="name_prize[]"],[name="date_received[]"]'));

                    $('#register-form4').find('.txttitle').each(function(e,s) {
                        $(s).text('Panel ' + (e+1))
                    });

                });

                $('#period_from').datepicker({
                    autoclose: true,
                    minViewMode: 1,
                    format: 'yyyy-mm'
                }).on('change',function(){
                    $('#register-form4').bootstrapValidator('updateStatus', 'period_from', 'VALID')
                });

                $('#period_to').datepicker({
                    autoclose: true,
                    minViewMode: 1,
                    format: 'yyyy-mm'
                }).on('change',function(){
                    $('#register-form4').bootstrapValidator('updateStatus', 'period_to', 'VALID')
                });

                $('.selectDate').datepicker({
                    autoclose: true,
                    format: "yyyy-mm-dd",
                    viewMode: 2
                })

                // $('#register-form4').bootstrapValidator(
                //     {
                //         message: 'This value is not valid',
                //         fields: {
                //             // 'period_from':{
                //             //     validators: {
                //             //         notEmpty: {
                //             //             message: 'The Period Attended From is  and cannot be empty'
                //             //         }
                //             //     }
                //             // },
                //             // 'period_to': {
                //             //     validators: {
                //             //         notEmpty:{
                //             //             message: 'The Period Attended To  is  and cannot be empty'
                //             //         }
                //             //     }
                //             // },
                //             'school_name':{
                //                 validators: {
                //                     notEmpty: {
                //                         message: 'The School is  and cannot be empty'
                //                     }
                //                 }
                //             },
                //             'state_province': {
                //                 validators: {
                //                     notEmpty: {
                //                         message: 'The State/Province is  and cannot be empty'
                //                     }
                //                 }
                //             },
                //             'country': {
                //                 validators: {
                //                     notEmpty: {
                //                         message: 'The Country is  and cannot be empty'
                //                     }
                //                 }
                //             },
                //             'relate_document': {
                //                 <?php if (!empty($info->relate_document)) echo 'excluded: true,'; ?>
                //                 validators: {
                //                     notEmpty: {
                //                         message: 'Please upload document'
                //                     },
                //                     file: {
                //                         extension: 'jpeg,jpg,pdf',
                //                         type: 'image/jpeg,image/jpg,application/pdf',
                //                         maxSize: 2097152,
                //                         message: 'The selected file is not valid'
                //                     }
                //                 }
                //             },

                //             // 'type_competition[]': {
                //             //     validators: {
                //             //         notEmpty: {
                //             //             message: 'The Type of Competition is  and cannot be empty'
                //             //         }
                //             //     }
                //             // },
                //             // 'name_institution[]': {
                //             //     validators: {
                //             //         notEmpty: {
                //             //             message: 'The Name of Host Institution/Organization of Host is  and cannot be empty'
                //             //         }
                //             //     }
                //             // },
                //             // 'name_prize[]': {
                //             //     validators: {
                //             //         notEmpty: {
                //             //             message: 'The Name of The Prize/Award is  and cannot be empty'
                //             //         }
                //             //     }
                //             // },
                //             // 'date_received[]': {
                //             //     validators: {
                //             //         notEmpty: {
                //             //             message: 'The Date Received is  and cannot be empty'
                //             //         }
                //             //     }
                //             // }
                //         }
                //     }).on('error.validator.bv', function(e, data) {
                //         data.element
                //             .data('bv.messages')
                //             // Hide all the messages
                //             .find('.help-block[data-bv-for="' + data.field + '"]').hide()
                //             // Show only message associated with current validator
                //             .filter('[data-bv-validator="' + data.validator + '"]').show();
                //     });
            })
        </script>
