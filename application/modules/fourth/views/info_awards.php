<?php foreach($info_awards as $awards_k =>$awards_v){?>
<div class="panel-group">
    <div class="panel panel-primary">
        <div class="panel-heading">Panel <?php echo $awards_k+1;?>
            <button  class="close removeButton">X</button>
        </div>
        <div class="panel-body">
            <div class="form-row">
                <div class="form-group" style="margin-bottom: 10px;">
                    <label for="type_competition" class="radio-label">Type of competition :</label>
                    <div class="form-select">
                        <select name="type_competition[]" id="type_competition" style="padding:6px 20px; ">
                            <option value="">-- select one --</option>
                            <?php
                            foreach($type_competition_list as $tcvalue) {
                                echo '<option value="'.$tcvalue.'" '.set_select('type_competition', $tcvalue,((!empty($awards_v->type_competition) && $awards_v->type_competition==$tcvalue) ? TRUE : FALSE)).'>'.$tcvalue.'</option>';
                            }
                            ?>
                        </select>
                        <span class="select-icon"><i class="zmdi zmdi-chevron-down"></i></span>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label for="name_institution" class="radio-label">Name of host institution/organization :</label>
                    <input type="text" name="name_institution[]" style="padding:6px 20px; " id="name_institution" value="<?php echo set_value('name_institution', ((!empty($awards_v->name_institution)) ? $awards_v->name_institution : '')); ?>"/>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group" style="margin-bottom: 10px;">
                    <label for="name_prize" class="radio-label">Name of the prize/award :</label>
                    <input type="text" name="name_prize[]" style="padding:6px 20px; " id="name_prize" value="<?php echo set_value('name_prize', ((!empty($awards_v->name_prize)) ? $awards_v->name_prize : '')); ?>"/>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label for="date_received" class="radio-label">Date received:</label>
                    <input type="text" name="date_received[]" class="selectDate" autocomplete="off"  readonly  style="padding:6px 20px; " id="date_received" value="<?php echo set_value('date_received', ((!empty($awards_v->date_received)) ? $awards_v->date_received : '')); ?>"/>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }?>