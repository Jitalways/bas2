        <div class="container">
            <div class="signup-content">
                <div class="signup-form" style="margin:0px auto;">
                    <?php
                    if (isset($error)) {
                        echo '<div class="alert alert-danger">เกิดความผิดพลาด! ไม่สามารถบันทึกข้อมูลลงระบบได้</div>';
                    } ?>
                    <form method="POST" action="<?php echo base_url(); ?>second" class="register-form" role="form" id="register-form2">
                        <h2>Application Form ( Step 2. )</h2>

                        <h4>
                            Biographical Information
                        </h4>
                        <div class="form-group">
                              <label for="name_eng" class="radio-label">Full Name in English :</label>
                              <input type="text" name="name_eng" id="name_eng" value="<?php echo set_value('name_eng', ((!empty($info->name_eng)) ? $info->name_eng : '')); ?>" required />
                          </div>
                          <div class="form-group">
                              <label for="name_thai" class="radio-label">Full Name in Thai(if applicable) :</label>
                              <input type="text" name="name_thai" id="name_thai" value="<?php echo set_value('name_thai', ((!empty($info->name_thai)) ? $info->name_thai : '')); ?>"/>
                          </div>
                          <div class="form-row">
                            <div class="form-group">
                                <label for="date_of_birth" class="radio-label">Date of Birth :</label>
                                <input type="text" name="date_of_birth" autocomplete="off"  readonly id="date_of_birth"  value="<?php echo set_value('date_of_birth', ((!empty($info->date_of_birth)) ? $info->date_of_birth : '')); ?>" required />
                            </div>
                            <div class="form-group">
                                <label for="place_of_birth" class="radio-label">Place of Birth :</label>
                                <input type="text" name="place_of_birth" id="place_of_birth" value="<?php echo set_value('place_of_birth', ((!empty($info->place_of_birth)) ? $info->place_of_birth : '')); ?>" required=""/>
                            </div>
                          </div>
                          <div class="form-group">
                              <label for="permanent_address" class="radio-label">Permanent Address :</label>
                              <input type="text" name="permanent_address" id="permanent_address" value="<?php echo set_value('permanent_address', ((!empty($info->permanent_address)) ? $info->permanent_address : '')); ?>" required />
                          </div>
                        <div class="form-row">
                          <div class="form-group">
                              <label for="province" class="radio-label">Province :</label>
                              <input type="text" name="province" id="province" value="<?php echo set_value('province', ((!empty($info->province)) ? $info->province : '')); ?>" required />
                          </div>
                          <div class="form-group">
                              <label for="district" class="radio-label">District :</label>
                              <input type="text" name="district" id="district" value="<?php echo set_value('district', ((!empty($info->district)) ? $info->district : '')); ?>" required />
                          </div>
                        </div>
                        <div class="form-row">
                          <div class="form-group">
                              <label for="zipcode" class="radio-label">Zipcode :</label>
                              <input type="text" name="zipcode" id="zipcode" value="<?php echo set_value('zipcode', ((!empty($info->zipcode)) ? $info->zipcode : '')); ?>" required />
                          </div>
                          <div class="form-group">
                              <label for="telephone" class="radio-label">Telephone : Ex.021234567</label>
                              <input type="text" name="telephone" id="telephone" value="<?php echo set_value('telephone', ((!empty($info->telephone)) ? $info->telephone : '')); ?>" required />
                          </div>
                        </div>
                        <div class="form-row">
                          <div class="form-group">
                              <label for="area_code" class="radio-label">Mobile Phone : Ex.0812345678</label>
                              <div class="form-row" style="margin: 0px 0px;">
                                <div class="form-select">
                                      <select name="area_code" id="area_code">
                                        <option value="">-- select one --</option>

                                          <?php
                                            foreach ($area_code_list as $avalue) {
                                                $code_value = $avalue->code . "\t" . $avalue->dial_code;
                                                echo '<option value="' . $code_value . '" ' . set_select('area_code', $code_value, ((!empty($info->area_code) && $info->area_code == $code_value) ? true : false)) . '>' . $code_value . '</option>';
                                            }
                                            ?>
                                      </select>
                                      <span class="select-icon"><i class="zmdi zmdi-chevron-down"></i></span>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="mobile" id="mobile" value="<?php echo set_value('mobile', ((!empty($info->mobile)) ? $info->mobile : '')); ?>" required />
                                </div>
                                </div>
                          </div>
                          <div class="form-group">
                          </div>
                        </div>
                        <div class="form-group">
                              <label for="mail_address" class="radio-label">Mailing Address (if different from above) :</label>
                              <input type="text" name="mail_address" id="mail_address" value="<?php echo set_value('mail_address', ((!empty($info->mail_address)) ? $info->mail_address : '')); ?>" />
                          </div>
                          <br/>
                          <hr/>
                        <h2>
                        Full Name in English of Contact Person
                        </h2>
                        <div class="form-row">
                          <div class="form-group">
                              <label for="title" class="radio-label">Title  :<?php echo $info->title_name_contact; ?></label>
                              <div class="form-select">
                                  <?php $title_name_list = array('Mr.', 'Ms.', 'Mrs.', 'Dr.'); ?>
                                  <select name="title_name_contact" id="title_name_contact">
                                      <option value="">-- select one --</option>
                                      <?php
                                        foreach ($title_name_list as $tvalue) {
                                            echo '<option value="' . $tvalue . '" ' . set_select('title_name_contact', $tvalue, ((!empty($info->title_name_contact) && $info->title_name_contact == $tvalue) ? true : false)) . '>' . $tvalue . '</option>';
                                        }
                                        ?>
                                  </select>
                                    <span class="select-icon"><i class="zmdi zmdi-chevron-down"></i></span>
                                </div>
                          </div>
                          <div class="form-group">
                              <label for="name_contact" >Name :</label>
                              <input type="text" name="name_contact" id="name_contact" value="<?php echo set_value('name_contact', ((!empty($info->name_contact)) ? $info->name_contact : '')); ?>" required />
                          </div>
                        </div>
                        <div class="form-group">
                              <label for="address_contact">Address  :</label>
                              <textarea name="address_contact" cols="40" rows="5" style="width:100%"><?php echo set_value('address_contact', ((!empty($info->address_contact)) ? $info->address_contact : '')); ?></textarea>
                          </div>
                        <div class="form-row">
                          <div class="form-group">
                              <label for="telephone_contact" class="radio-label">Telephone Number : Ex.021234567</label>
                              <input type="text" name="telephone_contact" id="telephone_contact" value="<?php echo set_value('telephone_contact', ((!empty($info->telephone_contact)) ? $info->telephone_contact : '')); ?>" required />
                          </div>
                          <div class="form-group">
                            <label for="mobile_contact" class="radio-label">Mobile Phone Number : Ex.0812345678</label>
                              <input type="text" name="mobile_contact" id="mobile_contact" value="<?php echo set_value('mobile_contact', ((!empty($info->mobile_contact)) ? $info->mobile_contact : '')); ?>" required />
                          </div>
                        </div>
                        <div class="form-row">
                          <div class="form-group">
                              <label for="relationship" class="radio-label">Relationship :</label>
                              <input type="text" name="relationship" id="relationship" value="<?php echo set_value('relationship', ((!empty($info->relationship)) ? $info->relationship : '')); ?>" required />
                          </div>
                          <div class="form-group">
                          </div>
                        </div>
                        <div class="form-submit">
                            <input type="button" value="Back" class="submit btn btn-default" id="reset" title="/first"/>
                            <input type="submit" value="Submit Form" class="submit btn btn-primary" />
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <script>



             $('#date_of_birth').datepicker({
                    autoclose: true,
                })
             .on('change',function(){
                    $('#register-form2').bootstrapValidator('updateStatus', 'date_of_birth', 'VALID')
                });;

            $(document).ready(function() {
                var cardtype_text = 'The card_no is not valid'
                $('#register-form2').bootstrapValidator(
                    {
                        message: 'This value is not valid',
                        fields: {
                            'name_eng':{
                                validators: {
                                    notEmpty: {
                                        message: 'The Full Name in English is required and cannot be empty'
                                    }
                                }
                            },
                            'date_of_birth': {
                                validators: {
                                    notEmpty:{
                                        message: 'The Date of Birth is required and cannot be empty'
                                    }
                                }
                            },
                            'place_of_birth':{
                                validators: {
                                    notEmpty: {
                                        message: 'The Place of Birth is required and cannot be empty'
                                    }
                                }
                            },
                            'permanent_address': {
                                validators: {
                                    notEmpty: {
                                        message: 'The Permanent Address is required and cannot be empty'
                                    }
                                }
                            },
                            'province': {
                                validators: {
                                    notEmpty: {
                                        message: 'The Province is required and cannot be empty'
                                    }
                                }
                            },
                            'district': {
                                validators: {
                                    notEmpty: {
                                        message: 'The District is required and cannot be empty'
                                    }
                                }
                            },
                            'zipcode': {
                                validators: {
                                    notEmpty: {
                                        message: 'The Zipcode is required and cannot be empty'
                                    }
                                }
                            },
                            'telephone': {
                                // validators: {
                                //     notEmpty: {
                                //         message: 'The Telephone is required and cannot be empty'
                                //     },
                                //     integer: {
                                //         message: 'The value is not an integer'
                                //     }
                                // }
                                message: 'The Telephone is required and cannot be empty',
                                validators: {
                                    callback: {
                                        // message: 'The Telephone Contact is required and cannot be empty',
                                        callback: function (value, validator, $field) {
                                                var cardno = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
                                                if(cardno.test(value)){
                                                   return true
                                                } else {
                                                    return {
                                                        valid: false,
                                                        message: 'The value is not valid'
                                                    }
                                                }
                                            }
                                        
                                    },
                                }
                            },
                            'area_code': {
                                validators: {
                                    notEmpty: {
                                        message: 'The Area Code is required and cannot be empty'
                                    }
                                }
                            },
                            'mobile': {
                                // validators: {
                                //     notEmpty: {
                                //         message: 'The Mobile is required and cannot be empty'
                                //     },
                                //     integer: {
                                //         message: 'The value is not an integer'
                                //     }
                                // }
                                message: 'The Mobile is required and cannot be empty',
                                validators: {
                                    callback: {
                                        // message: 'The Telephone Contact is required and cannot be empty',
                                        callback: function (value, validator, $field) {
                                                var cardno = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
                                                if(cardno.test(value)){
                                                   return true
                                                } else {
                                                    return {
                                                        valid: false,
                                                        message: 'The value is not valid'
                                                    }
                                                }
                                            }
                                        
                                    },
                                }
                            },
                            'title_name_contact': {
                                validators: {
                                    notEmpty: {
                                        message: 'The Title is required and cannot be empty'
                                    }
                                }
                            },
                            'name_contact': {
                                validators: {
                                    notEmpty: {
                                        message: 'The Name Contact is required and cannot be empty'
                                    }
                                }
                            },
                            'address_contact': {
                                validators: {
                                    notEmpty: {
                                        message: 'The Address Contact is required and cannot be empty'
                                    }
                                }
                            },
                            'telephone_contact': {
                                message: 'The Telephone Contact is required and cannot be empty',
                                validators: {
                                    // notEmpty: {
                                    //     message: 'The Telephone Contact is required and cannot be empty'
                                    // },
                                    // integer: {
                                    //     message: 'The value is not an integer'
                                    // },
                                    callback: {
                                        // message: 'The Telephone Contact is required and cannot be empty',
                                        callback: function (value, validator, $field) {
                                                var cardno = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
                                                if(cardno.test(value)){
                                                   return true
                                                } else {
                                                    return {
                                                        valid: false,
                                                        message: 'The value is not valid'
                                                    }
                                                }
                                            }
                                        
                                    },
                                }
                            },
                            'mobile_contact': {
                                // validators: {
                                //     notEmpty: {
                                //         message: 'The Mobile Contact is required and cannot be empty'
                                //     },
                                //     integer: {
                                //         message: 'The value is not an integer'
                                //     }
                                // }
                                message: 'The Mobile Contact is required and cannot be empty',
                                validators: {
                                    // notEmpty: {
                                    //     message: 'The Telephone Contact is required and cannot be empty'
                                    // },
                                    // integer: {
                                    //     message: 'The value is not an integer'
                                    // },
                                    callback: {
                                        // message: 'The Telephone Contact is required and cannot be empty',
                                        callback: function (value, validator, $field) {
                                                var cardno = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
                                                if(cardno.test(value)){
                                                   return true
                                                } else {
                                                    return {
                                                        valid: false,
                                                        message: 'The value is not valid'
                                                    }
                                                }
                                            }
                                        
                                    },
                                }
                            },
                            'relationship': {
                                validators: {
                                    notEmpty: {
                                        message: 'The Relationship is required and cannot be empty'
                                    }
                                }
                            }
                        }
                    })
                    .on('error.validator.bv', function(e, data) {
                data.element
                    .data('bv.messages')
                    // Hide all the messages
                    .find('.help-block[data-bv-for="' + data.field + '"]').hide()
                    // Show only message associated with current validator
                    .filter('[data-bv-validator="' + data.validator + '"]').show();
            });
                })
        </script>
