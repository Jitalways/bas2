<?php

class Thankyou extends MY_Controller
{
  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
     $this->view('thankyou');
  }

  public function next_step() {
      $this->_removeCookie(array('member'));
      redirect('first');
  }

}
