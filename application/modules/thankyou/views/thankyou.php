<div class="container">
            <div class="signup-content">
                  <div class="signup-form" style="width: 100% !important;">
                    <div  class="register-form">
                      <div class="panel-heading">
                        <h2 class="text-center">
                          Thank you
                        </h2>
                        
                        </div>
                        <div class="panel-body panel-primary text-center">
                            The application form has been completed
                        </div>
                    </div>
                  </div>
            </div>
</div>

<script>
    window.setTimeout(function() {
        window.location.href = '<?php echo base_url();?>thankyou/next_step';
    }, 5000);

</script>