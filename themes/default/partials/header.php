<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>

  <link rel="stylesheet" href="<?php echo base_url(); ?>themes/default/fonts/material-icon/css/material-design-iconic-font.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>themes/default/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>themes/default/css/style.css?v=1.0">
  <link rel="stylesheet" href="<?php echo base_url(); ?>themes/default/vendor/datepicker/css/datepicker.min.css">
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css"> -->
  <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css">
  <!-- JS -->
  <script src="<?php echo base_url(); ?>themes/default/js/jquery-3.3.1.min.js"></script>
  <script src="<?php echo base_url(); ?>themes/default/vendor/daterangepicker/moment.min.js"></script>
  <script src="<?php echo base_url(); ?>themes/default/vendor/datepicker/js/bootstrap-datepicker.min.js"></script>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js" ></script> -->
  <script src="<?php echo base_url(); ?>themes/default/vendor/validator/js/bootstrapValidator.js"></script>
  <script src="<?php echo base_url(); ?>themes/default/js/main.js"></script>

  <style>
    .signup-form{
        /* width:80% */
    }
    textarea {
      border: 1px solid #ebebeb;
      width:100%; 
      font-family: 'Montserrat'; 
      resize: none;
      font-weight: 500;
      padding: 5px;
    }
    h1,h3,h4,h5{
      font-family: 'Montserrat';
      font-weight: 700;
    }
  </style>
</head>
<body>
    <div class="main">